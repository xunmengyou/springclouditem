package com.wwy.schedule.Schedule;

import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.*;

@Component
public class ScheduledThreadPoolServer {

    //开启一个定时任务线程池
    private static ScheduledExecutorService scheduler = new ScheduledThreadPoolExecutor(5);
    //存放正在使用的定时任务
    private static ConcurrentHashMap<String,Future> futureMap = new ConcurrentHashMap<>();

    public Integer addTask(final String key,final String value){
        //如果任务已经存在线程池，那么不重复添加
        if(futureMap.contains(key)) return -1;
        /**
         * initialDelay 表示初始化延迟
         * period 表示两次执行最小时间间隔
         */
        Future future = scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                //执行具体的任务功能
                try {
                    System.out.println("任务: "+key+"  线程id: "+Thread.currentThread().getName()+"  时间start:"+new Date());
                    Thread.sleep(7000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("任务: "+key+"  线程id: "+Thread.currentThread().getName()+"  时间end:"+new Date());
            }
        },0,5,TimeUnit.SECONDS);
        //任务放入Map
        if(!futureMap.contains(key)) futureMap.put(key,future);
        return 1;
    }

    public Integer addTaskV2(final String key,final String value){
        //如果任务已经存在线程池，那么不重复添加
        if(futureMap.contains(key)) return -1;
        /**
         * initialDelay 首次执行的延迟时间
         * delay  一次执行终止和下一次执行开始之间的延迟
         *
         * -----和scheduleAtFixedRate区别：是任务上一次执行完后，才开始计算延迟。
         */
        Future future = scheduler.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                //执行具体的任务功能
                try {
                    System.out.println("FixedDelay任务: "+key+"  线程id: "+Thread.currentThread().getName()+"  时间start:"+new Date());
                    Thread.sleep(7000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("FixedDelay任务: "+key+"  线程id: "+Thread.currentThread().getName()+"  时间end:"+new Date());
            }
        },0,5,TimeUnit.SECONDS);
        //任务放入Map
        if(!futureMap.contains(key)) futureMap.put(key,future);
        return 1;
    }

    public void removeTask(String key){
        Future future = futureMap.get(key);
        //直接取消线程池中的任务
        future.cancel(true);
        futureMap.remove(key);
    }

    public static void main(String[] args) {

//        ScheduledThreadPoolServer scheduledThreadPoolServer = new ScheduledThreadPoolServer();
//        for(int i=0;i<3;i++) {
//            scheduledThreadPoolServer.addTaskV2("task " + String.valueOf(i), String.valueOf(i));
//        }

        ScheduledThreadPoolServer scheduledThreadPoolServer = new ScheduledThreadPoolServer();

        for(int i=0;i<3;i++){
            scheduledThreadPoolServer.addTask("task "+String.valueOf(i),String.valueOf(i));
            /**
             *  ------------initialDelay =0，period =5，coreSize=5，任务数是3，任务执行7秒-----
             * 任务: task 2  线程id: pool-1-thread-2  时间start:Mon Jan 10 17:31:29 CST 2022
             * 任务: task 0  线程id: pool-1-thread-1  时间start:Mon Jan 10 17:31:29 CST 2022
             * 任务: task 1  线程id: pool-1-thread-3  时间start:Mon Jan 10 17:31:29 CST 2022
             * 任务: task 2  线程id: pool-1-thread-2  时间end:Mon Jan 10 17:31:36 CST 2022
             * 任务: task 0  线程id: pool-1-thread-1  时间end:Mon Jan 10 17:31:36 CST 2022
             * 任务: task 0  线程id: pool-1-thread-2  时间start:Mon Jan 10 17:31:36 CST 2022
             * 任务: task 2  线程id: pool-1-thread-1  时间start:Mon Jan 10 17:31:36 CST 2022
             * 任务: task 1  线程id: pool-1-thread-3  时间end:Mon Jan 10 17:31:36 CST 2022
             * 任务: task 1  线程id: pool-1-thread-3  时间start:Mon Jan 10 17:31:36 CST 2022
             * 任务: task 0  线程id: pool-1-thread-2  时间end:Mon Jan 10 17:31:43 CST 2022
             * 任务: task 2  线程id: pool-1-thread-1  时间end:Mon Jan 10 17:31:43 CST 2022
             * 任务: task 2  线程id: pool-1-thread-1  时间start:Mon Jan 10 17:31:43 CST 2022
             *
             * 从这上面分析【scheduleAtFixedRate】：
             *   initialDelay =0，period =5，coreSize=5，任务执行时常，按照上面打印看出，同一个任务如果确定开始执行，
             *   那么开始计时延迟，如果在period范围内执行完，那么下一个延迟时间到来继续执行，如果当前任务
             *   执行超过period，那么这个任务结束后立马执行。----同一时刻不会并发调用任务。
             *
             *  -----------initialDelay =0，period =5，coreSize=2，任务数是3，任务执行7秒-----
             * 任务: task 1  线程id: pool-1-thread-2  时间start:Mon Jan 10 19:21:30 CST 2022
             * 任务: task 0  线程id: pool-1-thread-1  时间start:Mon Jan 10 19:21:30 CST 2022
             * 任务: task 0  线程id: pool-1-thread-1  时间end:Mon Jan 10 19:21:37 CST 2022
             * 任务: task 1  线程id: pool-1-thread-2  时间end:Mon Jan 10 19:21:37 CST 2022
             * 任务: task 2  线程id: pool-1-thread-1  时间start:Mon Jan 10 19:21:37 CST 2022
             * 任务: task 0  线程id: pool-1-thread-2  时间start:Mon Jan 10 19:21:37 CST 2022
             * 任务: task 2  线程id: pool-1-thread-1  时间end:Mon Jan 10 19:21:44 CST 2022
             * 任务: task 1  线程id: pool-1-thread-1  时间start:Mon Jan 10 19:21:44 CST 2022
             * 任务: task 0  线程id: pool-1-thread-2  时间end:Mon Jan 10 19:21:44 CST 2022
             * 任务: task 2  线程id: pool-1-thread-2  时间start:Mon Jan 10 19:21:44 CST 2022
             * 任务: task 2  线程id: pool-1-thread-2  时间end:Mon Jan 10 19:21:51 CST 2022
             * 任务: task 1  线程id: pool-1-thread-1  时间end:Mon Jan 10 19:21:51 CST 2022
             * 任务: task 0  线程id: pool-1-thread-2  时间start:Mon Jan 10 19:21:51 CST 2022
             * 任务: task 1  线程id: pool-1-thread-1  时间start:Mon Jan 10 19:21:51 CST 2022
             *
             *  从这上面分析【scheduleAtFixedRate】：
             *  由于线程池核心数 < 任务数，那么实际上只能处理两个任务，其余在队列排队。
             *
             *
             */
        }
        try {
            Thread.sleep(1000*60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
