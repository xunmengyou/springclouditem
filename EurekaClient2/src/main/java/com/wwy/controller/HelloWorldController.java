/**
 * 
 */
package com.wwy.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/basic")
public class HelloWorldController {

	//http://localhost:8084/basic/sayHello.do
	@RequestMapping("/sayHello.do")
	@ResponseBody //这是返回json格式
	public Object sayHello() {
		return "Hello,World!";
	}

	@RequestMapping("/wrong.html")
	public Object wrongPage(){
		return "wrong";
	}


}
