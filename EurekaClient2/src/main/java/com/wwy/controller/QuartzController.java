package com.wwy.controller;

import com.alibaba.fastjson.JSON;
import com.wwy.entity.JobInfo;
import com.wwy.quartz.JobHandler;
import com.wwy.quartz.TestV1Job;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class QuartzController {

    @Autowired
    TestV1Job testV1Job;

    @Autowired
    JobHandler jobHandler;

    @RequestMapping("/startTest1")
    @ResponseBody
    String startTest1(){
        testV1Job.startFirstJob();
        return "";
    }
    /**
     * http://localhost:8084/start?className=TimeEventJob&jobGroup=group1&jobName=job1&triName=trigger1&triGroup=group1
     *
     * http://localhost:8084/start?className=TimeEventJob&jobGroup=group1&jobName=job1&triName=trigger2&triGroup=group2
     */
    @RequestMapping("/start")
    @ResponseBody
    String start(String className,String jobName,String jobGroup,String triName,String triGroup) {
        JobInfo jobInfo = new JobInfo();
        jobInfo.setJobGroup(jobGroup); //group1
        jobInfo.setJobName(jobName);  //job1
        jobInfo.setCron("0/5 * * * * ?");
        jobInfo.setTriggerName(triName); //trigger1
        jobInfo.setTriggerGroup("trigger "+triGroup);  //trigger group1
        jobInfo.setClassName("com.wwy.quartz."+className); //"com.wwy.quartz.RemindJob"
        try {
            jobHandler.addJob(jobInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "1";
    }

    @RequestMapping("/modify")
    public @ResponseBody String modify(String cron,String jobGroup, String jobName,String triGroup, String triName) throws SchedulerException {
        jobHandler.modifyJob(cron,jobGroup,jobName,"trigger "+triGroup,triName);
        return "1"; //任务1会修改成1秒钟执行一次
    }

    @RequestMapping("/status")
    @ResponseBody
    public String status(String triGroup, String jobName) throws SchedulerException {
        return jobHandler.getJob1Status(triGroup,jobName);  //可以查看运行状态
    }

    @RequestMapping("/getJobInfo")
    @ResponseBody
    public Object getJobInfo(String jobGroup, String jobName) throws SchedulerException {
        return JSON.toJSONString(jobHandler.getJobInfo(jobGroup,jobName));
    }

    @RequestMapping("/pause")
    public @ResponseBody String pause(String jobGroup, String jobName) throws SchedulerException {
        jobHandler.pauseJob(jobGroup,jobName);  //任务会暂停
        return "1";
    }

    @RequestMapping("/resume")
    public @ResponseBody String resume(String jobGroup, String jobName) throws SchedulerException {
        jobHandler.continueJob(jobGroup,jobName);  //任务会重新运行
        return "1";
    }
}
