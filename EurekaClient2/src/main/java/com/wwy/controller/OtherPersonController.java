package com.wwy.controller;

import com.alibaba.fastjson.JSON;
import com.wwy.entity.PersonPo;
import com.wwy.service.Impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

/**
 * 作为Feign的服务提供类
 */
@RestController
public class OtherPersonController implements OtherPersonApi {

    @Value("${server.port}")
    private String port;

    @Autowired
    private PersonServiceImpl personServiceImpl;

    ////直接访问 http://localhost:7081/getPersonById.do?id=1
    //通过zuul配置访问 http://localhost:8081/EurekaClient2/getPersonById.do?id=1
    @Override
    public String getPersonById(Integer id) {
//       int i=1/0;
        PersonPo person = personServiceImpl.getPersonById(id);
        return "eurekaClient2 port:"+port+" getPersonById: "+JSON.toJSONString(person);
    }

    @Override
    public String getPersonById1(Integer id) {
        PersonPo person = personServiceImpl.getPersonById(id);
        return "eurekaClient2 port:"+port+" getPersonById1: "+JSON.toJSONString(person);
    }

    @Override
    public PersonPo insertPerson(Integer userId, PersonPo person) {
         person.setPersonID(userId);
         personServiceImpl.insertPerson(String.valueOf(userId),person);
        return  personServiceImpl.getPersonById(userId);
    }
}
