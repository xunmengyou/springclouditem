package com.wwy.controller;

import com.wwy.entity.PersonPo;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public interface OtherPersonApi {

    /**
     * 不支持@GetMapping @PostMapping，只能用@RequestMapping，通过method指定请求方式；
     * 参数传递必须用@RequestParam(value = "id") 注解修饰，传递的参数为对象，必须用@RequestBody修饰；
     * 返回值若为对象，对象必须序列化；
     */
    @RequestMapping(value = "/getPersonById.do",method = RequestMethod.GET)
    String getPersonById(@RequestParam(value = "id") Integer id);

    @RequestMapping(value = "/getPersonById1",method = RequestMethod.GET)
    String getPersonById1(@RequestParam(value = "id") Integer id);

    @RequestMapping(value = "/insertPerson.do",method = RequestMethod.GET)
    PersonPo insertPerson(@RequestParam(value = "userId") Integer userId,@RequestBody PersonPo person);
}
