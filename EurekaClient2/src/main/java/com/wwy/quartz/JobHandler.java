package com.wwy.quartz;

import com.alibaba.fastjson.JSON;
import com.wwy.entity.JobInfo;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class JobHandler {

    Logger log = LoggerFactory.getLogger(JobHandler.class);
    @Resource
    private Scheduler scheduler;

    /**
     * 添加任务
     *  支持一个任务添加多个Trigger
     */
    @SuppressWarnings("unchecked")
    public void addJob(JobInfo jobInfo) throws SchedulerException, ClassNotFoundException {
        //生成job key
        JobKey jobKey = JobKey.jobKey(jobInfo.getJobName(),jobInfo.getJobGroup());
        //已经存在的job，需要进一步判断
        if(scheduler.checkExists(jobKey)){
            log.warn("JobHandler add Job 已经存在 jobInfo:"+ JSON.toJSONString(jobInfo));
            return;
            //已经存在的，只要调度器方法不一样，那么可以重复添加 【目前不支持多个Trigger，后续研究下】
//            List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
//           if(!triggers.isEmpty()){
//               //继续判断，是否有相同的，只要没有相同的，那么就跳过
//               for(Trigger te : triggers){
//                   if(te.getKey().getName().equals(jobInfo.getTriggerName())
//                           && te.getKey().getGroup().equals(jobInfo.getTriggerGroup()))  return; //只要有相同，直接返回
//               }
//           }
        }
        //具体的添加job且调度器信息
        addDetail(jobInfo);
    }

    /**
     * 默认情况下JobDetail和Trigger是存储在内存中的，如果想要持久化到数据库中，可以新增quartz.properties，修改配置准备数据库脚本。
     * @param jobInfo
     * @throws ClassNotFoundException
     * @throws SchedulerException
     */
    public void addDetail(JobInfo jobInfo) throws ClassNotFoundException, SchedulerException {
        JobKey jobKey = JobKey.jobKey(jobInfo.getJobName(),jobInfo.getJobGroup());
        Class<Job> jobClass = (Class<Job>)Class.forName(jobInfo.getClassName());
        // JobDetail任务明细
        JobDetail jobDetail = JobBuilder
                .newJob(jobClass)
                .withIdentity(jobKey)
                .withIdentity(jobInfo.getJobName(), jobInfo.getJobGroup())
                .withDescription(jobInfo.getJobName())
                .build();
        // 配置信息
        jobDetail.getJobDataMap().put("config", jobInfo.getConfig());
            // 定义触发器
        TriggerKey triggerKey = TriggerKey.triggerKey(jobInfo.getTriggerName(), jobInfo.getTriggerGroup());
        // Trigger
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerKey)
                .withSchedule(CronScheduleBuilder.cronSchedule(jobInfo.getCron()))
                .build();
        //调度
        scheduler.scheduleJob(jobDetail, trigger);
    }

    /**
     * 任务暂停
     */
    public void pauseJob(String jobGroup, String jobName) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        if (scheduler.checkExists(jobKey)) {
            scheduler.pauseJob(jobKey);
        }
    }

    /**
     * 继续任务
     */
    public void continueJob(String jobGroup, String jobName) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        if (scheduler.checkExists(jobKey)) {
            scheduler.resumeJob(jobKey);
        }
    }

    /**
     * 修改任务的 cron表达式
     */
    public void modifyJob(String cron,String jobGroup, String jobName,String triGroup, String triName) throws SchedulerException {
        //新的调度器
        CronTrigger newTrigger = TriggerBuilder.newTrigger().withIdentity(triName, triGroup)
                .withSchedule(CronScheduleBuilder.cronSchedule(cron)).build();

        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        if (scheduler.checkExists(jobKey)) {
            scheduler.rescheduleJob(TriggerKey.triggerKey(triName, triGroup), newTrigger);
        }
    }

    /**
     * job任务状态
     */
    public String getJob1Status(String triGroup, String jobName) throws SchedulerException {
        TriggerKey triggerKey = TriggerKey.triggerKey(jobName, triGroup);
        return scheduler.getTriggerState(triggerKey).name();
    }

    /**
     * 删除任务
     */
    public boolean deleteJob(String jobGroup, String jobName) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        if (scheduler.checkExists(jobKey)) {
            return scheduler.deleteJob(jobKey);
        }
        return false;
    }

    /**
     * 获取任务信息
     */
    public List<JobInfo> getJobInfo(String jobGroup, String jobName) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        if (!scheduler.checkExists(jobKey)) {
            return null;
        }
        List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
        if (Objects.isNull(triggers)) {
            throw new SchedulerException("未获取到触发器信息");
        }
        List<JobInfo> jobList = new ArrayList<>();
        for(Trigger temp : triggers){
            JobInfo jobInfo = new JobInfo();
            jobInfo.setJobName(jobGroup);
            jobInfo.setJobGroup(jobName);
            jobInfo.setTriggerName(temp.getKey().getName());
            jobInfo.setTriggerGroup(temp.getKey().getGroup());

            JobDetail jobDetail = scheduler.getJobDetail(jobKey);
            jobInfo.setClassName(jobDetail.getJobClass().getName());
            if (Objects.nonNull(jobDetail.getJobDataMap())) {
                jobInfo.setConfig(JSON.toJSONString(jobDetail.getJobDataMap()));
            }
            Trigger.TriggerState triggerState = scheduler.getTriggerState(temp.getKey());
            jobInfo.setStatus(triggerState.toString());
            jobInfo.setCron(((CronTrigger)temp).getCronExpression());
            jobList.add(jobInfo);
        }
        return jobList;
    }

}
