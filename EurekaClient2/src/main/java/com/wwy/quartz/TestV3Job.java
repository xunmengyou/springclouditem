package com.wwy.quartz;

import com.wwy.entity.JobInfo;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * test3对应TestV3Job类
 */
public class TestV3Job implements Job {

    @Autowired
    JobHandler jobHandler;
    public void startCurJob(){
        JobInfo jobInfo = new JobInfo();
        jobInfo.setCron("0/5 * * * * ?");
        jobInfo.setJobGroup("groupTest3"); //group1
        jobInfo.setJobName("nameTest3");  //job1
        jobInfo.setTriggerName("triggerTest3"); //trigger1
        jobInfo.setTriggerGroup("triggerTest3 group ");  //trigger group1
        jobInfo.setClassName("com.wwy.quartz.TestV3Job");
        try {
            jobHandler.addJob(jobInfo);
        } catch (SchedulerException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(Thread.currentThread()+"TestV3Job任务，定时执行start: "+new Date());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        stopJob();
        System.out.println(Thread.currentThread()+"TestV3Job任务，定时执行end: "+new Date());
    }

    //暂停当前Job
    public void stopJob(){
        JobInfo jobInfo = new JobInfo();
        jobInfo.setJobGroup("groupTest3"); //group1
        jobInfo.setJobName("nameTest3");  //job1
        jobInfo.setCron("0/5 * * * * ?");
        jobInfo.setTriggerName("triggerTest3"); //trigger1
        jobInfo.setTriggerGroup("triggerTest3 group ");  //trigger group1
        jobInfo.setClassName("com.wwy.quartz.TestV3Job");
        try {
            jobHandler.pauseJob("groupTest3","nameTest3");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
