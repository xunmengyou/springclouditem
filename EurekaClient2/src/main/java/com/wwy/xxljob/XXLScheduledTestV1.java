package com.wwy.xxljob;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class XXLScheduledTestV1 extends IJobHandler {

    @XxlJob("scheduledTest1")
    public ReturnT<String> scheduledTest1(){
        //处理业务
        System.out.println(Thread.currentThread()+" scheduledTest1任务， time:"+new Date());
        return ReturnT.SUCCESS;
    }
    @Override
    public void execute() throws Exception {
        System.out.println(Thread.currentThread()+" XXLScheduledTestV1， execute() :"+new Date());
    }
}
