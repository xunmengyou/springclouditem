package com.wwy.service;

import com.wwy.entity.UserInfoPo;
import com.wwy.entity.UserPo;

import java.util.Set;

public interface UserInfoPoService {
    public void insertPerson1(UserInfoPo pers1);
    public UserInfoPo selectPerson1ById(Integer id);
    public Integer delPerson1(UserInfoPo pers1);
    public UserPo selectUser(String userName);
    public Set<String> userPerm(String userName);
}
