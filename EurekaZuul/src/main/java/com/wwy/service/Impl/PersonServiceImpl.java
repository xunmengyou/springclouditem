package com.wwy.service.Impl;

import com.wwy.dao.PersonMapper;
import com.wwy.entity.PersonPo;
import com.wwy.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonMapper personMapper;

    @Override
    public PersonPo getPersonById(Integer id) {
        PersonPo person = personMapper.selectPersonById(id);
        return person;
    }

    /**
     * 某个用户 插入的记录
     * @param userId
     * @param person
     */
    @Override
    public void insertPerson(String userId,PersonPo person){
        personMapper.insertPerson(person);
    }

    @Override
    public void delPerson(String userId, PersonPo person) {
         personMapper.delPerson(person);
    }


}
