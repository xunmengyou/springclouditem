package com.wwy.service.Impl;

import com.wwy.dao.UserInfoMapper;
import com.wwy.entity.UserInfoPo;
import com.wwy.entity.UserPo;
import com.wwy.service.UserInfoPoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserInfoServiceImpl implements UserInfoPoService {
    @Autowired
    UserInfoMapper userInfoMapper;

    @Override
    public void insertPerson1(UserInfoPo pers1) {
        userInfoMapper.insertPerson1(pers1);
    }

    @Override
    public UserInfoPo selectPerson1ById(Integer id) {
        return userInfoMapper.selectPerson1ById(id);
    }

    @Override
    public Integer delPerson1(UserInfoPo pers1) {
        return userInfoMapper.delPerson1(pers1);
    }

    @Override
    public UserPo selectUser(String userName) {
        //模拟下
        UserPo userPo = new UserPo();
        userPo.setUserName("admin");
        userPo.setPassWord("123456");
        return userPo;
    }

    @Override
    public Set<String> userPerm(String userName) {
        Set<String> permSet = new HashSet<>();
        //配置的权限，模拟查库
        permSet.add("user:show");
        permSet.add("user:admin");
        return permSet;
    }
}
