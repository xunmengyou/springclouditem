package com.wwy.learnitem;

import com.wwy.shiro.MyShiroFilterFactoryBean;
import com.wwy.shiro.MyShiroRealm;
import com.wwy.shiro.RemAndPermFilter;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.session.mgt.eis.MemorySessionDAO;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    @Bean
    public MyShiroRealm customRealm() {
        //用户数据和Shiro数据交互的桥梁。比如需要用户身份认证、权限认证。都是需要通过Realm来读取数据。
        MyShiroRealm customRealm = new MyShiroRealm();
//        // 告诉realm,使用credentialsMatcher加密算法类来验证密文
//        customRealm.setCredentialsMatcher(hashedCredentialsMatcher());
//        customRealm.setCachingEnabled(false);
        return customRealm;
    }

    @Bean
    public DefaultWebSecurityManager securityManager() {
        DefaultWebSecurityManager defaultSecurityManager = new DefaultWebSecurityManager();
        defaultSecurityManager.setRealm(customRealm());
//        //cookie生效时间设置【好像没效果】
//        defaultSecurityManager.setRememberMeManager(rememberMeManager());
        return defaultSecurityManager;
    }

    @Bean(name = "shiroFilter")
    public ShiroFilterFactoryBean shiroFilter(DefaultWebSecurityManager securityManager){
        ShiroFilterFactoryBean factoryBean = new MyShiroFilterFactoryBean();
        factoryBean.setSecurityManager(securityManager);
        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
        factoryBean.setLoginUrl("/login");  //这是当任何url时，都进入/login界面
        // 登录成功后要跳转的连接
        factoryBean.setSuccessUrl("/index");   //当验证成功后，会进入/index
        //未授权界面、权限不足跳转页面，跳转到403
        factoryBean.setUnauthorizedUrl("/403");  //对应MyShiroRealm类的返回null时，自动跳转到/403
        //加载ShiroFilter权限控制规则
        loadShiroFilterChain(factoryBean);
//        //加入自定义的filter
//        loadShiroFilter(factoryBean);
        return factoryBean;
    }

    /**
     * 开启shiro权限注解
     */
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor() {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager());
        return authorizationAttributeSourceAdvisor;
    }

    /**
     * shiro明文加密
     */
    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher(){
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("md5");//散列算法:这里使用MD5算法;
        hashedCredentialsMatcher.setHashIterations(2);//散列的次数，比如散列两次，相当于 md5(md5(""));
        // storedCredentialsHexEncoded默认是true，此时用的是密码加密用的是Hex编码；false时用Base64编码
        hashedCredentialsMatcher.setStoredCredentialsHexEncoded(true);
        return hashedCredentialsMatcher;
    }
    /**
     * 加载ShiroFilter权限控制规则
     */
    private void loadShiroFilterChain(ShiroFilterFactoryBean factoryBean) {

        Map<String, String> filterChainMap = new LinkedHashMap<String, String>();
        /**
         * anno、authc、authcBasic、logout、noSessionCreation、perms、port、rest、roles、ssl、user过滤器
         * anon是配置不会被拦截的连接，所有url都都可以匿名访问；
         * authc是需要认证的；
         * user: 配置记住我或认证通过可以访问
         */
        filterChainMap.put("/login", "anon");
        //	filterChainMap.put("/register", "anon");
        //配置退出  过滤器
        filterChainMap.put("/logout", "logout");
        //剩余请求需要身份认证
        filterChainMap.put("/**", "authc");
        factoryBean.setFilterChainDefinitionMap(filterChainMap);
    }


//    @Bean
//    @DependsOn({"lifecycleBeanPostProcessor"})
//    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
//        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
//        advisorAutoProxyCreator.setProxyTargetClass(true);
//        return advisorAutoProxyCreator;
//    }



//    @Bean
//    public SessionDAO getSessionDao() {
//        return new MemorySessionDAO();
//    }

//    @Bean
//    public SimpleCookie rememberMeCookie() {
//        // 这个参数是cookie的名称，对应前端的checkbox的name = rememberMe
//        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
//        // <!-- 记住我cookie生效时间30天 ,单位秒;-->
//        simpleCookie.setMaxAge(10);
//        return simpleCookie;
//    }
//    @Bean
//    public CookieRememberMeManager rememberMeManager() {
//        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
//        cookieRememberMeManager.setCookie(rememberMeCookie());
//        // rememberMe cookie加密的密钥 建议每个项目都不一样 默认AES算法 密钥长度(128 256 512 位)
//        cookieRememberMeManager.setCipherKey(Base64.decode("2AvVhdsgUs0FSA3SDFAdag=="));
//        return cookieRememberMeManager;
//    }
//    @Bean
//    public DefaultWebSecurityManager securityManager(){
//        DefaultWebSecurityManager securityManager =  new DefaultWebSecurityManager();
//        //用户的权限认证
//        securityManager.setRealm(myShiroRealm());
//        //cookie生效时间设置
//        securityManager.setRememberMeManager(rememberMeManager());
//        return securityManager;
//    }
//
//    @Bean
//    public MyShiroRealm myShiroRealm(){
//        MyShiroRealm myShiroRealm = new MyShiroRealm();
//        myShiroRealm.setCredentialsMatcher(hashedCredentialsMatcher());
//        return myShiroRealm;
//    }
//
//    @Bean(name = "shiroFilter")
//    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager) {
//        ShiroFilterFactoryBean factoryBean = new MyShiroFilterFactoryBean();
//        factoryBean.setSecurityManager(securityManager);
//        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
//        factoryBean.setLoginUrl("/login");  //这是当任何url时，都进入/login界面
//        // 登录成功后要跳转的连接
//        factoryBean.setSuccessUrl("/index");   //当验证成功后，会进入/index
//        //未授权界面，跳转到403
//        factoryBean.setUnauthorizedUrl("/403");  //对应MyShiroRealm类的返回null时，自动跳转到/403
//        //加载ShiroFilter权限控制规则
//        loadShiroFilterChain(factoryBean);
//        //加入自定义的filter
//        loadShiroFilter(factoryBean);
//        return factoryBean;
//    }

//    /**
//     * 加入自定义的filter
//     *
//     * @param factoryBean
//     */
//    private void loadShiroFilter(ShiroFilterFactoryBean factoryBean) {
//
//        Map<String, Filter> filterMap = new LinkedHashMap<String, Filter>();
//        filterMap.put("authc", new RemAndPermFilter());  //登陆权限过滤
//        factoryBean.setFilters(filterMap);
//    }
}
