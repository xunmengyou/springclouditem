package com.wwy.dao;

import com.wwy.entity.PersonPo;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonMapper {
    public void insertPerson(PersonPo personPo);
    public PersonPo selectPersonById(Integer id);
    public Integer delPerson(PersonPo personPo);
}
