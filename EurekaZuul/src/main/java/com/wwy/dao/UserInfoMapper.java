package com.wwy.dao;

import com.wwy.entity.UserInfoPo;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoMapper {
    public void insertPerson1(UserInfoPo pers1);
    public UserInfoPo selectPerson1ById(Integer id);
    public Integer delPerson1(UserInfoPo pers1);
}
