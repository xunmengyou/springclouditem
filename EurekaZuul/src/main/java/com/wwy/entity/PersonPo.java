package com.wwy.entity;

import java.io.Serializable;

public class PersonPo implements Serializable {
    private static final long serialVersionUID = 1262155676185874135L;
    private Integer PersonID;
    private String LastName;
    private String FirstName;
    private String Address;
    private String City;

    public Integer getPersonID() {
        return PersonID;
    }

    public void setPersonID(Integer personID) {
        PersonID = personID;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    @Override
    public String toString(){
        return "PersonID:"+PersonID+",LastName:"+LastName+",FirstName:"+FirstName+
                ",Address:"+Address+",City:"+City;
    }

    private String testReflect(){
        return "OK";
    }
}
