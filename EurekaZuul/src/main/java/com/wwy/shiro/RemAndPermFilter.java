package com.wwy.shiro;

import com.wwy.entity.UserPo;
import com.wwy.service.UserInfoPoService;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 登录权限过滤
 */
public class RemAndPermFilter extends AuthorizationFilter {
    @Autowired
    UserInfoPoService userInfoPoService;

    public RemAndPermFilter() {
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        Subject subject = getSubject(servletRequest, servletResponse);

        // 如果 已经登录验证过
        if (subject.isAuthenticated()||subject.isRemembered()) {
            HttpServletRequest httpRrequest = (HttpServletRequest) servletRequest;
            String path = httpRrequest.getServletPath(); //这是浏览器传来的path
            UserPo user=new UserPo();
            //由浏览器获得登陆名
            user.setUserName(subject.getPrincipal().toString());
            //查询数据库得到这个用户可以获得权限 userInfoPoService类去查询
            Map<String,String> map= getPermissionMap(); //这是模拟的
            //	return "remAndPerm".equals(map.get(path));
            return "authc".equals(map.get(path));

        }
        return subject.isAuthenticated() || subject.isRemembered();
        //	return true;  //测试controller时用true，浏览器就不会过滤(其实并没有影响)
    }
    public Map<String,String> getPermissionMap(){
        Map<String,String> map =new HashMap<String,String>(16);
        map.put("/index","remAndPerm");
        map.put("/log","remAndPerm");
        return map;
    }
}
