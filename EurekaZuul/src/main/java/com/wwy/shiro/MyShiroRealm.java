package com.wwy.shiro;

import com.wwy.entity.UserInfoPo;
import com.wwy.entity.UserPo;
import com.wwy.service.Impl.UserInfoServiceImpl;
import com.wwy.service.UserInfoPoService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

/**
 * shiro认证
 */
public class MyShiroRealm extends AuthorizingRealm {

    @Autowired
    UserInfoServiceImpl userInfoService;
    /**
     *  权限认证，即登录过后，每个身份不一定，对应的所能看的页面也不一样。
     *
     * 如果连续访问同一个URL（比如刷新），该方法不会被重复调用，Shiro有一个时间间隔（也就是cache时间，在ehcache-shiro.xml中配置），
     * 超过这个时间间隔再刷新页面，该方法会被执行
     *
     * doGetAuthorizationInfo()是权限控制， 当访问到页面的时候，使用了相应的注解或者shiro标签才会执行此方法否则不会执行，
     * 所以如果只是简单的身份认证没有权限的控制的话，那么这个方法可以不进行实现，直接返回null即可
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        String loginName = (String) super.getAvailablePrincipal(principalCollection);
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //查询mysql获取用户信息
        UserPo userInfo = userInfoService.selectUser(loginName);
        if(userInfo != null){
            //权限信息，用于存放用户的所有的角色及权限
//            info.setRoles(new HashSet<>()); //角色信息
            info.setStringPermissions(userInfoService.userPerm(loginName));//用户对应的所有权限
            SecurityUtils.getSubject().getSession().setAttribute("user", userInfo);
        }
        return info;
    }
    /**
     * 身份认证。即登录通过账号和密码验证登陆人的身份信息
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        //查询mysql获取用户信息  通过浏览器提交的用户名去数据库查找相应的用户信息
        UserPo userInfo = userInfoService.selectUser(token.getUsername());
        if(userInfo != null){
            SecurityUtils.getSubject().getSession().setAttribute("user", userInfo);
            // 若存在，将此用户存放到登录认证info中，无需自己做密码对比，Shiro会为我们进行密码对比校验
            String userloginName= userInfo.getUserName();
            String userPasswd=userInfo.getPassWord();
            String name=getName();   //realm name
            return new SimpleAuthenticationInfo(userloginName, userPasswd, name);
        }
        return null;
    }
}
