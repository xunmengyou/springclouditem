package com.wwy.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class MyZuulFilter extends ZuulFilter {
    /**
     * 返回一个字符串代表过滤器的类型
     * pre：路由之前调用。实现身份验证
     * routing：路由之时，过滤器将请求路由到微服务。用于构建发送给微服务的请求，并使用Apache HttpClient或Netfilx Ribbon请求微服务
     * post： 路由之后，这种过滤器可用来为响应添加标准的HTTP Header、收集统计信息和指标、将响应从微服务发送给客户端等。
     * error：发送错误调用，在Zuul中生成响应，而不将请求转发到后端的微服务。
     */
    @Override
    public String filterType() {
        return "pre";
    }
    @Override
    public int filterOrder() { //数字越小表示顺序越高，越先执行
        return 0;
    }
    @Override
    public boolean shouldFilter() {
        return true; //表示是否需要执行该filter，true表示执行，false表示不执行
    }
    /**
     * filter需要执行的具体操作
     * http://localhost:8081/eurekaClient1/ribbonPersonById.do?token=2
     */
    @Override
    public Object run() throws ZuulException {
//        RequestContext ctx = RequestContext.getCurrentContext();
//        HttpServletRequest request = ctx.getRequest();
////        System.out.println(String.format("%s >>> %s", request.getMethod(), request.getRequestURL().toString()));
//        String accessToken = request.getParameter("token"); // 获取请求的参数
//        if(StringUtils.isNotBlank(accessToken)) {
//            //进行路由
//            ctx.setSendZuulResponse(true);
//            ctx.setResponseStatusCode(200);
//            ctx.set("isSuccess", true);
//        }else{
//            //不进行路由
//            ctx.setSendZuulResponse(false);
//            ctx.setResponseStatusCode(400);
//            ctx.setResponseBody("token is empty");
//            ctx.set("isSuccess", false);
//        }
        return null;
    }
}
