package com.wwy.schedule;

import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

//@Component
public class MultiThreadScheduleTaskServer {

    @Scheduled(cron = "0/1 * * * * *")
    public void multiThreadCronV1(){
        System.out.println("multiThreadCronV1  "+Thread.currentThread().getName()+" 执行时间start:"+new Date());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0/1 * * * * *")
    public void multiThreadCronV2(){
        System.out.println("multiThreadCronV2  "+Thread.currentThread().getName()+" 执行时间start:"+new Date());
    }

    @Scheduled(cron = "0/1 * * * * *")
    public void asyncCronV3(){
        System.out.println("multiThreadCronV3 "+Thread.currentThread().getName()+" 执行时间start:"+new Date());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * ---------------coreSize大于num(task)-----------
     * multiThreadCronV2  pool-1-thread-2 执行时间start:Mon Jan 10 13:52:32 CST 2022
     * multiThreadCronV3 pool-1-thread-2 执行时间start:Mon Jan 10 13:52:32 CST 2022
     * multiThreadCronV1  pool-1-thread-1 执行时间start:Mon Jan 10 13:52:32 CST 2022
     * multiThreadCronV2  pool-1-thread-3 执行时间start:Mon Jan 10 13:52:33 CST 2022
     * multiThreadCronV2  pool-1-thread-4 执行时间start:Mon Jan 10 13:52:34 CST 2022
     * multiThreadCronV3 pool-1-thread-4 执行时间start:Mon Jan 10 13:52:35 CST 2022
     * multiThreadCronV2  pool-1-thread-2 执行时间start:Mon Jan 10 13:52:35 CST 2022
     * multiThreadCronV2  pool-1-thread-2 执行时间start:Mon Jan 10 13:52:36 CST 2022
     * multiThreadCronV2  pool-1-thread-2 执行时间start:Mon Jan 10 13:52:37 CST 2022
     * multiThreadCronV2  pool-1-thread-2 执行时间start:Mon Jan 10 13:52:38 CST 2022
     * multiThreadCronV1  pool-1-thread-3 执行时间start:Mon Jan 10 13:52:38 CST 2022
     * multiThreadCronV3 pool-1-thread-4 执行时间start:Mon Jan 10 13:52:38 CST 2022
     * multiThreadCronV2  pool-1-thread-1 执行时间start:Mon Jan 10 13:52:39 CST 2022
     * multiThreadCronV2  pool-1-thread-1 执行时间start:Mon Jan 10 13:52:40 CST 2022
     * multiThreadCronV2  pool-1-thread-2 执行时间start:Mon Jan 10 13:52:41 CST 2022
     * multiThreadCronV3 pool-1-thread-5 执行时间start:Mon Jan 10 13:52:41 CST 2022
     *
     * 结论和自己管理线程是一样的。
     */
}
