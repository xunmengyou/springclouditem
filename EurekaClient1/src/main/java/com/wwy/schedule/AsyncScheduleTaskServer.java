package com.wwy.schedule;

import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//@Component
public class AsyncScheduleTaskServer {

    private static ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
            2,2,1000,TimeUnit.SECONDS,new LinkedBlockingDeque<>(100));

    @Scheduled(cron = "0/1 * * * * *")
    public void asyncCronV1(){
        poolExecutor.execute(()->{
            System.out.println("async  CronV1 "+Thread.currentThread().getName()+" 执行时间start:"+new Date());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int a =1/0; //给一个异常
        });
    }

    @Scheduled(cron = "0/1 * * * * *")
    public void asyncCronV2(){
        poolExecutor.execute(()->{
            System.out.println("async  CronV2 "+Thread.currentThread().getName()+" 执行时间start:"+new Date());
        });
    }

    @Scheduled(cron = "0/1 * * * * *")
    public void asyncCronV3(){
        poolExecutor.execute(()->{
            System.out.println("async  CronV3 "+Thread.currentThread().getName()+" 执行时间start:"+new Date());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Scheduled(cron = "0/1 * * * * *")
    public void asyncCronV4(){
        poolExecutor.execute(()->{
            System.out.println("async  CronV4 "+Thread.currentThread().getName()+" 执行时间start:"+new Date());
        });
    }

    /**
     * ---------------coreSize大于num(task)-----------
     *async  CronV1 pool-1-thread-1 执行时间start:Mon Jan 10 11:59:14 CST 2022
     * async  CronV2 pool-1-thread-2 执行时间start:Mon Jan 10 11:59:14 CST 2022
     * async  CronV2 pool-1-thread-3 执行时间start:Mon Jan 10 11:59:15 CST 2022
     * async  CronV1 pool-1-thread-2 执行时间start:Mon Jan 10 11:59:15 CST 2022
     * async  CronV2 pool-1-thread-3 执行时间start:Mon Jan 10 11:59:16 CST 2022
     * async  CronV1 pool-1-thread-3 执行时间start:Mon Jan 10 11:59:16 CST 2022
     * async  CronV2 pool-1-thread-4 执行时间start:Mon Jan 10 11:59:17 CST 2022
     * async  CronV1 pool-1-thread-4 执行时间start:Mon Jan 10 11:59:17 CST 2022
     * Exception in thread "pool-1-thread-1" java.lang.ArithmeticException: / by zero
     * Exception in thread "pool-1-thread-2" java.lang.ArithmeticException: / by zero
     * async  CronV2 pool-1-thread-5 执行时间start:Mon Jan 10 11:59:18 CST 2022
     * async  CronV1 pool-1-thread-5 执行时间start:Mon Jan 10 11:59:18 CST 2022
     *
     * 分析：我们定义的线程池是3个，任务是2个。
     *  CronV1或CronV2的结果看出，丢给线程池调用，只要线程池coreSize够了，那么都是并行执行。
     *
     *  因此存在问题：
     *  (1)、每个schedule执行不受上一个任务是否完成，照样执行。
     *  (2)、即使出现异常，后续任务照样执行。
     *
     *
     *  ---------------coreSize小于num(task)-----------
     *  async  CronV3 pool-1-thread-1 执行时间start:Mon Jan 10 13:34:15 CST 2022
     * async  CronV2 pool-1-thread-2 执行时间start:Mon Jan 10 13:34:15 CST 2022
     * async  CronV1 pool-1-thread-2 执行时间start:Mon Jan 10 13:34:15 CST 2022
     * async  CronV4 pool-1-thread-1 执行时间start:Mon Jan 10 13:34:17 CST 2022
     * async  CronV4 pool-1-thread-1 执行时间start:Mon Jan 10 13:34:17 CST 2022
     * async  CronV2 pool-1-thread-1 执行时间start:Mon Jan 10 13:34:17 CST 2022
     * async  CronV3 pool-1-thread-1 执行时间start:Mon Jan 10 13:34:17 CST 2022
     * async  CronV1 pool-1-thread-3 执行时间start:Mon Jan 10 13:34:18 CST 2022
     * Exception in thread "pool-1-thread-2" java.lang.ArithmeticException: / by zero
     * async  CronV4 pool-1-thread-1 执行时间start:Mon Jan 10 13:34:19 CST 2022
     * async  CronV2 pool-1-thread-1 执行时间start:Mon Jan 10 13:34:19 CST 2022
     * async  CronV3 pool-1-thread-1 执行时间start:Mon Jan 10 13:34:19 CST 2022
     * Exception in thread "pool-1-thread-3" java.lang.ArithmeticException: / by zero
     * async  CronV1 pool-1-thread-1 执行时间start:Mon Jan 10 13:34:21 CST 2022
     * async  CronV4 pool-1-thread-4 执行时间start:Mon Jan 10 13:34:21 CST 2022
     *
     * 存在问题：
     * (1)、多线程的coreSize数设置不够，那么任务也会在队列中排列等待执行。
     */

}
