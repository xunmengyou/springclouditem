package com.wwy.schedule;

import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

//@Component
public class ScheduleTaskServer {

    /**
     * cron 每10秒执行
     */
    @Scheduled(cron = "0/1 * * * * *")
    public void cronV1(){
        System.out.println("cronV1 "+Thread.currentThread().getName()+" 执行时间start:"+new Date());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0/1 * * * * *")
    public void cronV2(){
        System.out.println("cronV2 "+Thread.currentThread().getName()+" 执行时间start:"+new Date());
    }

    /**
     * cronV2 scheduling-1 执行时间start:Mon Jan 10 11:18:35 CST 2022
     * cronV1 scheduling-1 执行时间start:Mon Jan 10 11:18:35 CST 2022
     * cronV2 scheduling-1 执行时间start:Mon Jan 10 11:18:40 CST 2022
     * cronV1 scheduling-1 执行时间start:Mon Jan 10 11:18:41 CST 2022
     * cronV2 scheduling-1 执行时间start:Mon Jan 10 11:18:46 CST 2022
     * cronV1 scheduling-1 执行时间start:Mon Jan 10 11:18:47 CST 2022
     * cronV2 scheduling-1 执行时间start:Mon Jan 10 11:18:52 CST 2022
     * cronV1 scheduling-1 执行时间start:Mon Jan 10 11:18:53 CST 2022

     看上述结果分析：

     即使开启两个@Scheduled，也是同一个线程【scheduling-1】执行，说明@Scheduled注解是单线程。
     本身 cronV2先执行完，然后等待cronV1执行完，才能执行cronV2。
     存在问题：
     (1)、单线程执行，如果某个任务阻塞会影响其他任务。
     */
}
