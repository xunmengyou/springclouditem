package com.wwy.service;

import com.wwy.entity.PersonPo;
import com.wwy.service.Impl.FeignPersonServiceeHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 通过feign实现外部访问其他服务
 * 没成功【后续查原因】
 */
@FeignClient(value = "EurekaClient2",fallback = FeignPersonServiceeHystrix.class)
public interface FeignPersonService {

    @RequestMapping(value = "/getPersonById1",method = RequestMethod.GET)
    String getPersonById1(@RequestParam(value = "id") Integer id);

    @RequestMapping(value = "/otherPerson/insertPerson.do",method = RequestMethod.GET)
    PersonPo insertPerson(@RequestParam(value = "userId") Integer userId,@RequestBody PersonPo person);
}
