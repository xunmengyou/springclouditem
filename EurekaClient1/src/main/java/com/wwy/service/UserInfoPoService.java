package com.wwy.service;

import com.wwy.entity.UserInfoPo;

public interface UserInfoPoService {
    public void insertPerson1(UserInfoPo pers1);
    public UserInfoPo selectPerson1ById(Integer id);
    public Integer delPerson1(UserInfoPo pers1);
}
