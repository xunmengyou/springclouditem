package com.wwy.service;

import com.wwy.entity.PersonPo;

public interface PersonService {
    PersonPo getPersonById(Integer id);
    void insertPerson(String userId, PersonPo person);
    void delPerson(String userId, PersonPo person);

}
