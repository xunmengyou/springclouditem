package com.wwy.service.Impl;

import com.wwy.dao.UserInfoMapper;
import com.wwy.entity.UserInfoPo;
import com.wwy.service.UserInfoPoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl implements UserInfoPoService {
    @Autowired
    UserInfoMapper userInfoMapper;

    @Override
    public void insertPerson1(UserInfoPo pers1) {
        userInfoMapper.insertPerson1(pers1);
    }

    @Override
    public UserInfoPo selectPerson1ById(Integer id) {
        return userInfoMapper.selectPerson1ById(id);
    }

    @Override
    public Integer delPerson1(UserInfoPo pers1) {
        return userInfoMapper.delPerson1(pers1);
    }
}
