package com.wwy.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RibbonPersonService {

    @Autowired
    RestTemplate restTemplate;
    @HystrixCommand(fallbackMethod = "personByIdHytrix")
    public String getPersonById(Integer id){
       // http://localhost:7081/otherPerson/getPersonById.do?id=1
        String result = restTemplate.getForObject(
                "http://EurekaClient2/getPersonById.do?id="+id,String.class);
        return result;
    }
    //熔断的功能
    public String personByIdHytrix(Integer id){
        return "personById Hytrix";
    }
}
