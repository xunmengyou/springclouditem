package com.wwy.learnitem;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *若打包成war包，使用外置的tomcat启动，那么需要继承SpringBootServletInitializer，并重写
 * configure()方法
 *
 * public class LearnitemApplication extends SpringBootServletInitializer {
 */
@EnableDiscoveryClient //注解注册到服务中心
@EnableFeignClients(basePackages={"com.wwy.service.*"})      //feign注解
@EnableEurekaClient
@EnableHystrix
@SpringBootApplication(scanBasePackages= {"com.wwy"})
@MapperScan(basePackages = {"com.wwy.*"}) //可以测试各个层
@EnableScheduling  //开启定时任务功能
public class LearnitemApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder){
        return builder.sources(LearnitemApplication.class);
    }


    public static void main(String[] args) {
        SpringApplication.run(LearnitemApplication.class, args);
    }

}
