package com.wwy.controller;

import com.alibaba.fastjson.JSON;
import com.wwy.entity.PersonPo;
import com.wwy.service.Impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/eurekaClient1")
public class PersonController {

    @Autowired
    private PersonServiceImpl personServiceImpl;

    //http://localhost:8084/person/getPersonById.do
    @RequestMapping("/getPersonById.do")
    @ResponseBody
    public Object getPersonById(){
        PersonPo person = personServiceImpl.getPersonById(1);
        return JSON.toJSONString(person);
    }
}
