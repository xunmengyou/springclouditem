package com.wwy.controller;

import com.wwy.service.FeignPersonService;
import com.wwy.service.RibbonPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class OtherPersonController {

    @Autowired
    RibbonPersonService ribbonPersonService;

    @Autowired
    FeignPersonService feignPersonService;
 //直接访问  http://localhost:7080/ribbonPersonById.do
    //通过zuul配置后，跳转访问 http://localhost:8081/eurekaClient1/ribbonPersonById.do
    @RequestMapping("/ribbonPersonById.do")
    @ResponseBody
    public Object ribbonPersonById(){
        String person = ribbonPersonService.getPersonById(1);
        return person;
    }

    //http://localhost:7080/otherPersonById.do
    @RequestMapping("/feignPersonByIdV1.do")
    @ResponseBody
    public Object feignPersonByIdV1(){
        String person = feignPersonService.getPersonById1(1);
        return person;
    }

}
