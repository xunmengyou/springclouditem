package com.wwy.controller;

import com.wwy.entity.PersonBasicPo;
import com.wwy.entity.UserPo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * 说明： 这里只是简单集成thymeleaf，完成登录页面跳转的功能
 */
@Controller
public class loginController {

    @RequestMapping("/loginV1.html")
    public String login(Model model){
        List<PersonBasicPo> list = new ArrayList<>();
        PersonBasicPo user = new PersonBasicPo("wwy","female","3232332");
        list.add(user);

        model.addAttribute(list);
        return "loginV1";
    }

    /**
     * 这是通过后台请求跳转到登录页面
     * @param mode
     * @return
     */
    @GetMapping("/login.html")
    public ModelAndView login(ModelAndView mode){
        mode.setViewName("login");
        return mode;
    }

    /**
     * 对登录的账号进行验证
     * @param modelAndView
     * @param userPo
     * @param bindingResult
     * @return
     */
    @PostMapping("/login")
    public ModelAndView login(ModelAndView modelAndView, @Valid UserPo userPo, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            modelAndView.addObject("error",bindingResult.getFieldError().getDefaultMessage());
            modelAndView.setViewName("login");
            return modelAndView;
        }
        String userName = userPo.getUserName();
        String password = userPo.getPassWord();

        if(!"admin".equals(userName) || !"123456".equals(password)){
            modelAndView.addObject("error","用户或密码错误！");
            modelAndView.setViewName("login");
            return modelAndView;
        }

        List<PersonBasicPo> list = new ArrayList<>();
        PersonBasicPo user = new PersonBasicPo(userName,password,"3232332");
        list.add(user);

        modelAndView.addObject("userEntityList",list);
//      modelAndView.addObject("userName",userName);
        modelAndView.setViewName("index"); //跳转到一个新页面
        return modelAndView;
    }

}
