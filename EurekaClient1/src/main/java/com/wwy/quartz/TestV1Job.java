package com.wwy.quartz;

import com.wwy.entity.JobInfo;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 模拟事件流转：  test1---test2----test3
 * test1对应TestV1Job类，这个类的要求是：如果正常执行完，那么转到下一个事件任务，否则重试n次后记录异常，并暂停事件任务
 */
@Component
@DisallowConcurrentExecution
public class TestV1Job implements Job {

    private static volatile Integer count = 0; //重试次数

    @Autowired
    JobHandler jobHandler;

    /**
     * Thread[quartzScheduler_Worker-1,5,main] TestV1Job任务，定时执行start: Fri Jan 14 16:22:05 CST 2022
     * Thread[quartzScheduler_Worker-1,5,main] TestV1Job任务，准备重试: 1 time:Fri Jan 14 16:22:07 CST 2022
     * Thread[quartzScheduler_Worker-2,5,main] TestV1Job任务，定时执行start: Fri Jan 14 16:22:10 CST 2022
     * Thread[quartzScheduler_Worker-2,5,main] TestV1Job任务，准备重试: 2 time:Fri Jan 14 16:22:12 CST 2022
     * Thread[quartzScheduler_Worker-3,5,main] TestV1Job任务，定时执行start: Fri Jan 14 16:22:15 CST 2022
     * TestV1Job任务，定时执行end: Fri Jan 14 16:22:17 CST 2022
     * Thread[quartzScheduler_Worker-4,5,main]TestV2Job任务，定时执行start: Fri Jan 14 16:22:20 CST 2022
     * Thread[quartzScheduler_Worker-4,5,main] TestV2Job任务，准备重试: 1 time:Fri Jan 14 16:22:25 CST 2022
     * Thread[quartzScheduler_Worker-5,5,main]TestV2Job任务，定时执行start: Fri Jan 14 16:22:25 CST 2022
     * Thread[quartzScheduler_Worker-5,5,main] TestV2Job任务，准备重试: 2 time:Fri Jan 14 16:22:30 CST 2022
     * Thread[quartzScheduler_Worker-6,5,main]TestV2Job任务，定时执行start: Fri Jan 14 16:22:30 CST 2022
     * Thread[quartzScheduler_Worker-6,5,main] TestV2Job任务，准备重试: 3 time:Fri Jan 14 16:22:35 CST 2022
     * Thread[quartzScheduler_Worker-7,5,main]TestV2Job任务，定时执行start: Fri Jan 14 16:22:35 CST 2022
     * TestV2Job任务，定时执行end: Fri Jan 14 16:22:40 CST 2022
     * Thread[quartzScheduler_Worker-8,5,main]TestV3Job任务，定时执行start: Fri Jan 14 16:22:42 CST 2022
     * TestV3Job任务，定时执行end: Fri Jan 14 16:22:44 CST 2022
     *
     * 从上面可以看出，整个流程随时暂停，随时可以启动
     */
    public void startFirstJob(){
        JobInfo jobInfo = new JobInfo();
        jobInfo.setJobGroup("groupTest1"); //group1
        jobInfo.setJobName("nameTest1");  //job1
        jobInfo.setCron("0/5 * * * * ?");
        jobInfo.setTriggerName("triggerTest1"); //trigger1
        jobInfo.setTriggerGroup("triggerTest1 group ");  //trigger group1
        jobInfo.setClassName("com.wwy.quartz.TestV1Job");
        try {
            jobHandler.addJob(jobInfo);
        } catch (SchedulerException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(Thread.currentThread()+" TestV1Job任务，定时执行start: "+new Date());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(count++<2){
            //重试5次
            System.out.println(Thread.currentThread()+" TestV1Job任务，准备重试: "+count+" time:"+new Date());
            return;
        }else{
            //直接暂停当前job
            stopJob();
        }
        startNextJob();
        System.out.println(Thread.currentThread()+" TestV1Job任务，定时执行end: "+new Date());
    }
    //开启下一个Job
    public void startNextJob(){
        JobInfo jobInfo = new JobInfo();
        jobInfo.setJobGroup("groupTest2"); //group1
        jobInfo.setJobName("nameTest2");  //job1
        jobInfo.setCron("0/5 * * * * ?");
        jobInfo.setTriggerName("triggerTest2"); //trigger1
        jobInfo.setTriggerGroup("triggerTest2 group ");  //trigger group1
        jobInfo.setClassName("com.wwy.quartz.TestV2Job");
        try {
            jobHandler.addJob(jobInfo);
        } catch (SchedulerException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    //暂停当前Job
    public void stopJob(){
        JobInfo jobInfo = new JobInfo();
        jobInfo.setJobGroup("groupTest1"); //group1
        jobInfo.setJobName("nameTest1");  //job1
        jobInfo.setCron("0/5 * * * * ?");
        jobInfo.setTriggerName("triggerTest1"); //trigger1
        jobInfo.setTriggerGroup("triggerTest1 group ");  //trigger group1
        jobInfo.setClassName("com.wwy.quartz.TestV1Job");
        try {
            jobHandler.pauseJob("groupTest1","nameTest1");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
