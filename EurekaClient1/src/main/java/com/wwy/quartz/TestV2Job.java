package com.wwy.quartz;

import com.wwy.entity.JobInfo;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * test2对应Test2Job类
 */
@Component
@DisallowConcurrentExecution //禁止并发执行多个相同定义的JobDetail,一次执行不完下次轮转时则等待改方法执行完后才执行下一次操作
public class TestV2Job implements Job {

    private static volatile Integer count = 0; //重试次数

    @Autowired
    JobHandler jobHandler;

    public void startCurJob(){
        JobInfo jobInfo = new JobInfo();
        jobInfo.setCron("0/5 * * * * ?");
        jobInfo.setJobGroup("groupTest2"); //group1
        jobInfo.setJobName("nameTest2");  //job1
        jobInfo.setTriggerName("triggerTest2"); //trigger1
        jobInfo.setTriggerGroup("triggerTest2 group ");  //trigger group1
        jobInfo.setClassName("com.wwy.quartz.TestV2Job");
        try {
            jobHandler.addJob(jobInfo);
        } catch (SchedulerException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(Thread.currentThread()+"TestV2Job任务，定时执行start: "+new Date());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(count++<3){
            System.out.println(Thread.currentThread()+" TestV2Job任务，准备重试: "+count+" time:"+new Date());
            return;
        }else
            stopJob();
        startNextJob();
        System.out.println(Thread.currentThread()+"TestV2Job任务，定时执行end: "+new Date());
    }

    //开启下一个Job
    public void startNextJob(){
        JobInfo jobInfo = new JobInfo();
        jobInfo.setJobGroup("groupTest3"); //group1
        jobInfo.setJobName("nameTest3");  //job1
        jobInfo.setCron("0/6 * * * * ?");
        jobInfo.setTriggerName("triggerTest3"); //trigger1
        jobInfo.setTriggerGroup("triggerTest3 group ");  //trigger group1
        jobInfo.setClassName("com.wwy.quartz.TestV3Job");
        try {
            jobHandler.addJob(jobInfo);
        } catch (SchedulerException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    //暂停当前Job
    public void stopJob(){
        JobInfo jobInfo = new JobInfo();
        jobInfo.setJobGroup("groupTest2"); //group1
        jobInfo.setJobName("nameTest2");  //job1
        jobInfo.setCron("0/5 * * * * ?");
        jobInfo.setTriggerName("triggerTest2"); //trigger1
        jobInfo.setTriggerGroup("triggerTest2 group ");  //trigger group1
        jobInfo.setClassName("com.wwy.quartz.TestV2Job");
        try {
            jobHandler.pauseJob("groupTest2","nameTest2");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
