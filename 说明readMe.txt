EurekaServer----用于注册服务

EurekaClient1--服务1
EurekaClient2---服务2   【只要Run/bug Configuration的springboot中配置修改，就可以启动多个服务】
EurekaClient3---服务3

1、三个服务启动后，都注册到Eureka server。

2、开始实现服务1-服务2直接互相调用。
    Hystrix+ribbon方式集成。

3、完成zuul组件集成。	

4、完成zuul+shiro组件集成。

